import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Row',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
        home: Scaffold(
        appBar: AppBar (
          title: const Text('My Row and Column'),
        ),
          body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      radius: 100,
                      backgroundImage: AssetImage('asset/IMG20190730150543.jpg'),
                  ),
                    Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Hello,',style: TextStyle(
                            fontSize:30
                              ),
                            ),
                          Text('Sarayut',style: TextStyle(
                            fontSize: 30
                              ),
                            ),
                       ],
                     ),
                   ),
                ],
              ),
            ),
         ],
      ),
        ),
    );
  }
}
